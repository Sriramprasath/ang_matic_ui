import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(private Api:ApiService) { }
 public username:String;
  ngOnInit() {
  this.username = atob(sessionStorage.getItem("user"))
  }
  logout(){
    this.Api.logout();
    console.log(this.Api.isLoggedOut(),"logout");
    console.log(this.Api.isLoggednIn(),"logged in")
  }
}
