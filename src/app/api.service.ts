import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

declare var require:any;
var utf8 = require('utf8');
const { note, proof, signer } = require('aztec.js');
//   // var importcrypt = require('cryptr');
import Cryptr from 'cryptr';
// const cryptr = new Cryptr('myTotalySecretKey');
import { Buffer } from "buffer";

import forge from 'node-forge';
import JSEncrypt from 'jsencrypt';
var Web3 = require('web3');
var testneturl="https://testnet2.matic.network/";
var web3 = new Web3(new Web3.providers.HttpProvider(testneturl));
var jsSHA = require("jssha");
var crypto_random_bits = 16;
var username;
var password;
var myprojectkey="matic123$";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public httpOptions:any;
  public baseurl:String="http://165.22.214.80:8000";
  public userInfo_baseurl:String="http://192.168.21.61:8000";
  public maticpublickey:any;
  public maticprivatekey:any; 
  public loader:boolean;  
  public menuhide:boolean;
  public current_router_url:string;
  constructor(private http:HttpClient,private router:Router,private ToastrService:ToastrService) {
  
    // this.httpOptions = {
    //   headers: new HttpHeaders({
    //     'Content-Type':'application/json',
    //    'Authorization': 'Token '+localStorage.getItem('token')          
    //  })
    // }

this.getRouterurl()

   }
getRouterurl(){
 this.current_router_url = this.router.url;
}

  encrypt_server_data(encrypted_data, privateKey) {
    const decrypt = new JSEncrypt({
      default_key_size: 2048
    })
    decrypt.setPrivateKey(privateKey)
  
    var i = 0;
    var plain_array = [];
    for (let i in encrypted_data) {
      const plainText = (decrypt.decrypt(encrypted_data[i]) || 'DECRYPTION FAILED')
      plain_array.push(plainText)
    }
    console.log(plain_array,"array")
    return JSON.parse(plain_array.join(''))  
  }

  genrate_Pbkdf2_key(msg, salt) {
    return forge.pkcs5.pbkdf2(msg, salt, 1000, 16);
  }

 symmetric_decrypt(cipertext, key) {
    let edata = atob(cipertext);
    let input = forge.util.createBuffer(edata.substring(16));
    let iv = forge.util.createBuffer(edata.substring(0, 16));
    var decipher = forge.cipher.createDecipher('AES-CBC', key);
    decipher.start({ iv: iv });
    decipher.update(input);
    decipher.finish();
    // console.log(decipher.output.toString());
    let plainText = decipher.output.toString();
    return plainText
  }
  

decrypt_response(response, session_key) {
  let encrypted_response = atob(response["data"]);
  let bytes_response = this.symmetric_decrypt(encrypted_response, session_key);
  let result = JSON.parse(bytes_response);
  return result
}


symmetric_encrypt(msg, key) {
  
    let iv = forge.random.getBytes(16);
    let input = forge.util.createBuffer(msg, 'utf8');
    var cipher = forge.cipher.createCipher('AES-CBC', key);
    cipher.start({ iv: iv });
    cipher.update(input);
    cipher.finish();
    // console.log(cipher.output.toHex());
    var encrypted = cipher.output;
    // console.log(encrypted)
    var encodedB64 = forge.util.encode64(iv + encrypted.data);
    console.log("Encoded: " + encodedB64);
    return encodedB64
  }
encrypt_request(second_data, session_key, session_uuid) {
    let data = JSON.stringify(second_data);
    let encrypted_request = this.symmetric_encrypt(data, session_key);
    let encrypted_encoded_request = btoa(encrypted_request);
    let new_request = {
      "data": encrypted_encoded_request,
      "session_uuid": session_uuid
    }
    return new_request
  
  }  

  pub_or_private_key(pub_pri_keys) {
    let body = window.btoa(String.fromCharCode(...new Uint8Array(pub_pri_keys)));
    body = body.match(/.{1,64}/g).join('\n');
    return body;
  }

  public async  createkeypair(){
    const options = {
      name: 'RSASSA-PKCS1-v1_5',
      modulusLength: 2048,
      publicExponent: new Uint8Array([0x01, 0x00, 0x01]),
      hash: { name: 'SHA-256' },
    };
    const keys = await window.crypto.subtle.generateKey(
      options,
      true, // non-exportable (public key still exportable)
      ['sign', 'verify'],
    );
    const publicKey = await window.crypto.subtle.exportKey('spki', keys.publicKey);
    const privateKey_1 = await window.crypto.subtle.exportKey('pkcs8', keys.privateKey);
    let body_pub = this.pub_or_private_key(publicKey)
    let body_private = this.pub_or_private_key(privateKey_1)
    var public_key = `-----BEGIN PUBLIC KEY-----\n${body_pub}\n-----END PUBLIC KEY-----`
    var private_key = `-----BEGIN PRIVATE KEY-----\n${body_private}\n-----END PRIVATE KEY-----`
     this.maticpublickey=public_key;
    this.maticprivatekey=private_key;
   }

public async createnode(pvt):Promise<any>{
  let aztecAccounts=  {
    "privateKey": "0x2eab689d3afa5aa86fe5829d2aaf7b844cc3a91a3ba84ad6397001103a635651",
    "publicKey": "0x04e34eab0be228dc4cbb314c6220f0f77442e716893376a936f7db5e51cb7809dfc100b08c308db489e54b9fdb9b069bb77b1b2513ec6dabdeb78c423235c1b572",
    "address": "0xCba39f71715820a3b352fE9A50d541168a1B1134"
}
var a  = await web3.eth.accounts.privateKeyToAccount(pvt);
console.log("pvtkey:",a.address)

let notes = [await note.create(aztecAccounts.publicKey, 100),
 await note.createZeroValueNote(),
 await note.create(aztecAccounts.publicKey, 100)]
 console.log(await notes,"creating")
 
// return new Promise((resolve,reject)=>{
  // let notes1 = [note.create(aztecAccounts.publicKey,100),
  //              note.createZeroValueNote(),
  //              note.create(pvt,100)]
               
  //              console.log(notes1,"responding data from api service")
          //  resolve(notes)     
// })
 // Create a note of value 100 for this asset such that it is owned by aztecAccount[1]
//  let notes = [await note.create(aztecAccounts[1].publicKey, 100),
//  await note.createZeroValueNote(),
//  await note.create(aztecAccounts[1].publicKey, 100)]

}
public async reg(data):Promise<any>{
  this.loader=true;
  let crypt_module = new Cryptr('aes256');
  console.log(data,"service ")
   let username = data["username"];
   let password = data["password"];
   let contact = data["contact"];
   let country = data["country"];
   let email = data["email"];

  var shaObj = new jsSHA("SHA-256", "TEXT");
  shaObj.update(data["password"]);
  var hash1 = shaObj.getHash("HEX");

  var sha3Obj = new jsSHA("SHA3-256", "TEXT");
  sha3Obj.update(data["password"]);
  var hash2 = sha3Obj.getHash("HEX");

  var encryptedpassword = crypt_module.encrypt(password,myprojectkey).toString();
  console.log(encryptedpassword,"encode ram")
  var decryptpassword = crypt_module.decrypt(encryptedpassword,myprojectkey).toString();
  console.log(decryptpassword,"decrpt ram")

  let seed = btoa(forge.random.getBytes(16))
  console.log(seed,"seed")
  let encrypted_seed=this.symmetric_encrypt(seed, this.genrate_Pbkdf2_key(password, password))


    let user_data = {
      "name": username,
      "email": email,
      "contact": contact,
      "country": country
      //  "balances": {
      //      "ETH": 20,
      //      "USD": 100000,
      //      "BTC": 20
      //  }
  }

  data = {
    "user_name": username,
    "user_data": user_data,
    "password_hash": hash1,
    "password_hash2": hash2,
    "seed": seed,
    "encrypted_seed":btoa(encrypted_seed),
      }

      return new Promise((reslove,reject)=>{
          this.http.post(this.baseurl+'/user/add/',data).subscribe(res =>{
            reslove(res)
            console.log(res,"res from register")
            if(res["status"] ==true){
              this.loader=false;
                this.ToastrService.success(res['data'],"Registration Success",{timeOut:3000});
                setTimeout(() => {
                  this.router.navigateByUrl('login')
                }, 2000);
            }else{
              this.loader=false;
              
              
                this.ToastrService.warning(res['data'],"Information",{timeOut:3000});
            }
          },
          error =>{
            this.loader=false;
            reslove(error)
            console.log(error,"error from register")
            this.ToastrService.error(error,"Error Registration",{timeOut:3000})
          })
      })as Promise<any>
}  

public async APIlogin(data):Promise<any>{
this.loader=true;
  this.createkeypair().finally(()=>{
    username = data["username"];
   password = data["password"].toString();
  var shaObj = new jsSHA("SHA-256", "TEXT");
  shaObj.update(data["password"]);
  var hash1 = shaObj.getHash("HEX");

  var sha3Obj = new jsSHA("SHA3-256", "TEXT");
  sha3Obj.update(data["password"]);
  var hash2 = sha3Obj.getHash("HEX");
 
    var P_key =btoa(this.maticpublickey);    
    var logincredicialsStep1={
    "user_name":username,
    "password_hash":hash1,
    "password_hash2":hash2,
    "public_key":P_key
  }
  console.log(logincredicialsStep1,"*******************")
  return new Promise((resolve,reject)=>{
    this.http.post(this.baseurl+'/user/login_front_end/',logincredicialsStep1).subscribe(res =>{
       console.log(res,"response &*&*&*&*&*&*&*&*&*&*&*&*&*&*&*&")
      sessionStorage.setItem("session_uuid",res['session_uuid']);
      this.ApIlogin2(res);
      resolve(res)
      if(!res['status'])
          {
            this.loader=false;
            this.ToastrService.warning(res['data'], 'Information Login', {
              timeOut: 3000
            });
            console.log("Naresh checks");
          }else{
            sessionStorage.setItem('user',btoa(username))
            this.loader=false;
            this.ToastrService.success("Login Success", 'Success Login', {
              timeOut: 3000
            });

            
          }
    },error =>{
      this.loader=false;
      this.ToastrService.error(error, 'Error Login', {
        timeOut: 3000
      });
      resolve(error)
    })
  }) as Promise<any>;
  })
}

  public async ApIlogin2(res){
      
        let response = this.encrypt_server_data(res["data"],this.maticprivatekey)
        // console.log("respose", response)
        let session_uuid = res["session_uuid"]
        let encrypted_seed = atob(response['encrypted_seed']);
        let new_seed = response['new_seed']
        let encoded_offset = response['offset']
        let session_key = response["session_key"]
    
        // console.log(encrypted_seed)
        const seed = this.symmetric_decrypt(encrypted_seed, this.genrate_Pbkdf2_key(password, password))
        const new_encrypted_seed = this.symmetric_encrypt(new_seed, this.genrate_Pbkdf2_key(password, password))
        // console.log(seed, new_encrypted_seed)
        session_key = atob(session_key)

        var second_data = {
          "seed": seed,
          "offset": response["offset"],
          "new_encrypted_seed": btoa(new_encrypted_seed)
        }
        var request_data = this.encrypt_request(second_data, session_key, session_uuid)
        return new Promise((resolve,reject)=>{
          this.http.post(this.baseurl+'/user/login2/',request_data).subscribe((res) =>{
            
            console.log(res,"response =========================");
            resolve(res)
            if(res){
              var data = {
                "offset": encoded_offset
              }
              console.log("--------------Login Success--------------");
              this.router.navigateByUrl('/dashboard')
              request_data = this.encrypt_request(data, session_key, session_uuid)
              
              // this.http.post(this.baseurl+'/user/get_balance/',request_data).subscribe((res) =>{
              //   var r_responce = this.decrypt_response(res, session_key)
              //   if(res !="" || res!= null){
              //     this.router.navigateByUrl('/menu')
              //   }
              //   // alert("--------------Login Success--------------");
              //   console.log("--------------Login Success--------------");
              //   console.log(r_responce, session_uuid, session_key, username)
              // })
            }

            
          },error =>{
            resolve(error)
          })
        }) as Promise<any>;
  }
  public async getAddress():Promise<any>{
    return new Promise((resolve,reject)=>{
      this.http.get(this.userInfo_baseurl+'/user/addresses/').subscribe(res =>{
        resolve(res);
      },
      error =>{
        resolve(error);
      })
    })as Promise<any>;
  }

  public async getCurrency(CUR):Promise<any>{
    return new Promise((resolve,reject)=>{
      this.http.get(this.userInfo_baseurl+'/user/addresses/'+CUR).subscribe(res =>{
        resolve(res);
      },
      error =>{
        resolve(error);
      })
    })as Promise<any>;
  }
  
    public async getNodehash(nodehash):Promise<any>{
      let data ={
        "asset_address":nodehash
      }
    return new Promise((resolve,reject)=>{
      this.http.post(this.userInfo_baseurl+'/get_note_hashes',data).subscribe(res =>{
        resolve(res);
      },
      error =>{
        resolve(error);
      })
    })as Promise<any>;
  }

  public async logout(){
    sessionStorage.clear();
    this.router.navigateByUrl('/login')
  }

  getAuth() {
    return sessionStorage.getItem("session_uuid")
  }
  isLoggednIn() {
    this.menuhide=false;
    return this.getAuth() !== null;
  }
  isLoggedOut() {
    this.menuhide=true;
    return this.getAuth() === null;
    
  }
}
